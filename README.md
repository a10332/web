# web

Petits programmes pour s'amuser avec html et plus. C'est essentiellement du « patentage » / « gossage » de code. Il n'y a pas vraiment de code « propre », mais il est suffisamment clair pour comprendre les rouages de la patente. Du moins, je l'espère.

## Liste des programmes
1. [changer_couleur.html](source/changer_couleur/changer_couleur.html) donne ![changer_couleur.gif](source/changer_couleur/changer_couleur.gif).
2. [echiquier.html](source/echiquier/echiquier.html) donne ![echiquier.gif](source/echiquier/echiquier.gif).
